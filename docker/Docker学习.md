## Docker学习

### 1、基本要求

<img src="1.png" style="zoom:150%;" />

### 2、Docker为什么出现

<img src="2.png" style="zoom:150%;" />



### 3、docker的历史

##### 	虚拟机和Docker 的区别

<img src="3.png" style="zoom:150%;" />

##### docker能干嘛

<img src="4.png" style="zoom:150%;" />



##### docker的基本组成

![](5.png)

![](6.png)

### 4、Dokcer的安装

参照官方文档： https://docs.docker.com/engine/install/centos/ 

![](7.png)

![](8.png)

![](9.png)

##### 阿里云镜像加速

![](10.png)

##### docker运行流程和底层原理

![](11.png)

![](12.png)

##### 为什么Dokcer 比VM快

<img src="13.png" style="zoom: 200%;" />

### 5、Docker的常用命令

##### 	帮助命令

![](14.png)

Docker官方文档： https://docs.docker.com/engine/reference/commandline/build/ 



##### 	镜像命令

###### 		docker pull

![](15.png)

<img src="16.png" style="zoom:120%;" />

###### 		删除镜像 docker rmi

<img src="17.png" style="zoom: 120%;" />

##### 	容器命令

###### 		docker ps -a

![](18.png)

###### 		docker rm、docker stop

![](19.png)

![](20.png)

###### 	 docker inspect 

<img src="21.png"  />

##### 常用的其他命令

###### 	 1、docker exec -it和 docker attach

<img src="22.png" style="zoom:150%;" />

###### 	2、从容器内拷贝文件到宿主机上（后面可以用数据卷 -v 参数）

![](23.png)

#### 命令总结脑图

![](25.png)

![](26.png)

![](27.png)

#### 作业练习

![](28.png)

##### 部署Nginx服务

![](29.png)

**docker run -d  -p 3389:80 --name nginx01 nginx**

**docker logs nginx01 -f --tail 20**    打印输出日志

##### 部署Tomcat镜像

![](30.png)

##### 部署ElasticSearch+kibana

```shell
加上内存限制才行，不然会启动不了，es很消耗内存的

#elasticsearch安装
mkdir -p ~/mydata/elasticsearch/data
mkdir -p ~/mydata/elasticsearch/plugins
echo "http.host: 0.0.0.0" >~/mydata/elasticsearch/config/elasticsearch.yml #冒号后面有空格的

给挂载目录赋予权限 chmod -R 777 elasticsearch/

docker run -d --name es01 -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" \
-e ES_JAVA_OPTS="-Xms64m -Xmx128m" \
-v ~/mydata/elasticsearch/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
-v ~/mydata/elasticsearch/data:/usr/share/elasticsearch/data \
-v ~/mydata/elasticsearch/plugins:/usr/share/elasticsearch/plugins \
elasticsearch:7.6.2

#kibana安装
docker run -d --name kb1 -e ELASTICSEARCH_HOSTS=http://localhost:9200 -p 5601:5601 kibana:7.6.2
```

![](31.png)

##### Kibana通过宿主机的内网ip连接es的

![](32.png)



##### Dokcer可视化部署

- **portainer**

**docker run -d -p 8089:9000 --restart=always -v /var/run/docker.sock:/var/run/docker.sock --privileged=true --name docker_view portainer/portainer**

- **Rancher**【CICD再用】

![](33.png)

##### 部署Mysql

**docker run -d -p 3308:3306 -v /home/mysql/conf:/etc/mysql/conf.d -v /home/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 --name mysql_v mysql:5.7**

![](42实战mysql.png)



### 6、Docker镜像讲解

![](34.png)

#### 	Docker镜像加载原理 

##### 	**bootfs 加载引导 和rootfs 最小虚拟机环境**

![](35.png)

#####  镜像分层理解 Layers

![](36.png)

![](37.png)

##### 	启动容器的过程解析

![](38.png)

#### Commit镜像

```shell
docker commit -m="提交测试的镜像" -a="testName" tom1 tomcat:1.0
```

![](39docker_commit.png)

## Dokcer进阶技术

### 7、容器数据卷Volume

![](40容器数据卷.png)

#### 	7.1使用数据卷【同步双向绑定】

![](41docke_inspect.png)

#### 7.2 具名和匿名挂载数据卷

```shell
docker run -d -P --name ng01 -v /etc/nginx nginx  //以随机端口,匿名挂载数据卷的方式启动nginx

docker run -d -P --name ng02 -v juming-ng:/etc/nginx nginx
376deba00a99b982f10c422c79086ee1e509a3e581b75dca3b997324c63921bc
[test1@taiqing docker]$ docker volume ls
DRIVER              VOLUME NAME
local               juming-ng
[test1@taiqing docker]$ docker volume inspect juming-ng
[
    {
        "CreatedAt": "2020-05-24T09:48:36+08:00",
        "Driver": "local",
        "Labels": null,
        "Mountpoint": "/var/lib/docker/volumes/juming-ng/_data",
        "Name": "juming-ng",
        "Options": null,
        "Scope": "local"
    }
]
```

![](4具名和匿名挂载.png)

![](44.png)

### 8、Dockerfile的知识点

![](45dockerfile.png)

> **VOLUME 指定的是容器内的目录**

![](46.png)



#### 容器内的数据共享 原理是备份

![](47容器内数据共享.png)

![](48.png)

#### 多个mysql容器实现数据同步

```shell
"$PWD/data":/var/lib/mysql #$PWD 当前用户的家目录

docker run -d -P -v "$PWD/data":/var/lib/mysql   -e MYSQL_ROOT_PASSWORD=root -v "$PWD/conf":/etc/mysql/conf.d  --name some-mysql mysql:5.7

mysql -uroot -p -h39.108.4.97 -P32774 #主机端口号远程登录mysql 阿里云控制台要开放端口号才能连接
```

![](49多个mysql数据同步.png)

#### Dockerfile的构建过程

![](50dockerfile的构建过程.png)

![](51.png)

#### Dokcerfile的指令

![](53指令说明.png)

![](52Dockerfile指令.png)



##### **案例demo**

![](54dockerfile_demo.png)

```shell
docker build -f centos_demo -t centosdemo:0.2 .
```

```shell

FROM centos
MAINTAINER test@qq.com

#键值对形式编写
ENV MYPATH /usr/local
WORKDIR $MYPATH

RUN yum install -y vim
RUN yum install -y net-tools

EXPOSE 80

CMD echo $MYPATH
CMD echo '-----------end---------'
CMD /bin/bash
```

##### **docker history  +镜像id** -->可查看镜像的构建过程**

##### CMD和ENTRYPOINT指令的区别

![](55cmd指令.png)

![](56entrypoint指令.png)



##### Docker push推送镜像到远程仓库

###### 	dockerhub方式

![](57.png)

###### 提交镜像出现的问题问题

![](58提交不上的原因.png)

###### 	阿里云提交镜像方式

![](59阿里云的提交镜像方式.png)

```shell
（必须是root权限，好坑！我一直用dev用户）
1. 登录阿里云Docker Registry

$ sudo docker login --username=qiutaiqing registry.cn-shenzhen.aliyuncs.com

用于登录的用户名为阿里云账号全名，密码为开通服务时设置的密码。
您可以在访问凭证页面修改凭证密码。
```

```shell

将镜像推送到Registry [可省略registry.cn-shenzhen.aliyuncs.com/]
$ docker tag [ImageId] registry.cn-shenzhen.aliyuncs.com/gzc-md/mp-test:[镜像版本号]
$  docker push registry.cn-shenzhen.aliyuncs.com/gzc-md/mp-test:[镜像版本号]
```



阿里云官方文档：https://cr.console.aliyun.com/repository/cn-shenzhen/gzc-md/mp-test/details



## 小结：Docker save和load指令 备份镜像到本地

![](60docker_save_load.png)

![](61.png)

```shell
docker save -o nginx.tar nginx:latest 
或 
docker save > nginx.tar nginx:latest 

docker load -i nginx.tar
或
docker load < nginx.tar
```

## Dockcer 网络

**有三个网卡**

![](62ip_addr.png)

**容器启动，宿主机会给容器分配一个网卡172.18.x.x**

![](63.png)

###### evth pair技术



![](64evth_pair网络技术.png)

##### 两个容器内部通信原理

![](65容器内部通信原理.png)



![](66.png)



##### --link 解决容器内部网络连通问题【本质是在hosts文件 加上容器名和ip】

![](67.png)

![](68.png)

### 自定义网络

```shell
[test1@qing tmp]$ docker  network create --driver bridge --subnet 192.168.0.0/16 --gateway 192.168.0.1 mynet
e5b9b2da774124f9c759b0ba6e149abbfeffb91f9d09db953c77a20752941028
[test1@qing tmp]$ docker network inspect mynet
```

![](69自定义网络.png)

#### 创建容器指定自定义的网络，相同容器内的网络可以互相ping通

![](70.png)

### 不同容器的网络也能ping通

把自定义网络，加入到不同网络的容器![](70.png)中

![](71容器加入与网络连通.png)

![](72.png)

## 高可用redis集群搭建

#### 创建redis过程的脚本

```shell
#创建网卡
[test1@qing tmp]$ docker network create redis --subnet 172.38.0.0/16
c547ba9b1c6c011ff5f615714f2941adb1a3dc89f23113a265accb258119a6a4
[test1@qing tmp]$ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
56a444cb6fbb        bridge              bridge              local
f60a349cbdf5        host                host                local
e5b9b2da7741        mynet               bridge              local
e2fd36f04517        none                null                local
c547ba9b1c6c        redis               bridge              local

创建redis集群脚本

for port in $(seq 1 6); \
do \
mkdir -p ~/mydata/redis/node-${port}/conf
touch ~/mydata/redis/node-${port}/conf/redis.conf
cat << EOF >~/mydata/redis/node-${port}/conf/redis.conf
port 6379
bind 0.0.0.0
cluster-enabled yes
cluster-config-file nodes.conf
cluster-node-timeout 5000
cluster-announce-ip 172.38.0.1${port}
cluster-announce-port 6379
cluster-announce-bus-port 16379
appendonly yes
EOF

docker run -p 637${port}:6379 -p 1637${port}:16379 --name redis-${port} \
-v ~/mydata/redis/node-${port}/data:/data \
-v ~/mydata/redis/node-${port}/conf/redis.conf:/etc/redis/redis.conf \
-d --net redis --ip 172.38.0.1${port} redis:5.0.9-alpine3.11 redis-server /etc/redis/redis.conf \

done

#创建集群

```



![](73高可用redis集群搭建.png)

![](74--脚本命令.png)

#### 进入redis容器 

```bash
docker exec -it redis-1 /bin/sh

redis-cli --cluster create 172.38.0.11:6379 172.38.0.12:6379 172.38.0.13:6379 172.38.0.14:6379 172.38.0.15:6379 172.38.0.16:6379  --cluster-replicas 1
```

![](75进入redis容器.png)

```shell
说明：
port ${PORT} ##节点端口
cluster-enabled yes ##cluster集群模式
cluster-config-file nodes.conf ##集群配置名
cluster-node-timeout 5000 ##超时时间
cluster-announce-ip 172.18.0.${TEMP} ##实际为各节点网卡分配ip 
cluster-announce-port ${PORT} ##节点映射端口
cluster-announce-bus-port 1${PORT} ##节点总线端
appendonly yes ##持久化模式 

# 集群不能用的情况：
（1）有半数或者半数以上的master挂掉，集群就不能用了
（2）如果集群任意master挂掉，且当前master没有slave，集群不能用
注：一个主库挂掉，它的从库自动顶替为主库，正常使用（前提是：有半数或者半数以上的master能用），挂掉的主库修复好后，会成为从库，不会抢占为主


# 进入容器后，以集群模式-c 连接redis 可指定[-h 172.38.0.13 -p 6379]
/data # redis-cli -c 

127.0.0.1:6379> cluster info
cluster_state:ok
cluster_slots_assigned:16384
cluster_slots_ok:16384
cluster_slots_pfail:0
cluster_slots_fail:0
cluster_known_nodes:6
cluster_size:3
cluster_current_epoch:6
cluster_my_epoch:1
cluster_stats_messages_ping_sent:513
cluster_stats_messages_pong_sent:516
cluster_stats_messages_sent:1029
cluster_stats_messages_ping_received:511
cluster_stats_messages_pong_received:513
cluster_stats_messages_meet_received:5
cluster_stats_messages_received:1029

127.0.0.1:6379> cluster nodes
9bf328ea5c5ecb105b0a2c76346c6df4bdeed24b 172.38.0.15:6379@16379 slave affe86cb7bc2012c978dacdd707e97a9c9644a21 0 1590316876000 5 connected
e4b3455d5101212b895b3cb68f10091f741e8cb9 172.38.0.13:6379@16379 master - 0 1590316876052 3 connected 10923-16383
0560fc8566a08bddd58dedcd4bb9781991a0c546 172.38.0.12:6379@16379 master - 0 1590316875000 2 connected 5461-10922
f681cde4f90ec5a2a0ce24759d4cdf118d04084c 172.38.0.16:6379@16379 slave 0560fc8566a08bddd58dedcd4bb9781991a0c546 0 1590316876152 6 connected
6337eecac4546339000db18fc9c10f09a790c08a 172.38.0.14:6379@16379 slave e4b3455d5101212b895b3cb68f10091f741e8cb9 0 1590316877056 4 connected
affe86cb7bc2012c978dacdd707e97a9c9644a21 172.38.0.11:6379@16379 myself,master - 0 1590316876000 1 connected 0-5460

```

## DockerFile部署springboot项目

```dockerfile
FROM java:8
COPY *.jar /app.jar
CMD ["--server.port=8082"]
EXPOSE 8082
ENTRYPOINT ["java","-jar","/app.jar"]
```

![](76idea_dockerfile.png)



```shell
#默认会找名为Dockerfile的文件进行构建
docker build -t thaiqing666:1.0 .

docker run -d -P --name springboot-captcha thaiqing666:1.0
```



#### docker tag 推送镜像的注意点                      

```shell
#             镜像ID              远程仓库地址:新的版本号
docker  tag  2869fc110bf7   registry.cn-shenzhen.aliyuncs.com/gzc-md/centos2diy:1.2
```



## Docker部署RabbitMQ

```shell
#选择镜像

 docker search rabbitmq:management
 
 docker pull rabbitmq:management

docker run -d -p 5672:5672 -p 15672:15672 --name rabbitmq rabbitmq:management

访问管理界面的地址就是 http://[宿主机IP]:15672，可以使用默认的账户登录，用户名和密码都guest，如：
```



## Docker部署单节点redis

```shell
docker pull redis

docker run --name my-redis -p 16379:6379 -v /usr/local/workspace/redis/data:/data -d redis redis-server --appendonly yes

#参数说明
--name my-redis         启动后容器名为 my-redis
-p 16379:3306        　　　 将容器的 3306 端口映射到主机的 16379 端口
-v  /usr/local/workspace/redis/data:/data       将主机/usr/local/workspace/redis/data  目录挂载到容器的 /data
redis-server --appendonly yes　　　 在容器执行redis-server启动命令，并打开redis持久化配置

# 进入redis容器
docker exec -it f9eb2360ed36 bash
# 找到redis执行脚本
root@f9eb2360ed36:/usr/local/bin# cd /usr/local/bin/
# 执行redis-cli命令连接到redis中
root@f9eb2360ed36:/usr/local/bin# ./redis-cli 

# 展示redis信息 
127.0.0.1:6379> info


#设置密码
为现有的redis创建密码或修改密码的方法：

1.进入redis的容器 docker exec -it 容器ID bash
2.进入redis目录 /usr/local/bin 
3.运行命令：redis-cli
4.查看现有的redis密码：config get requirepass
5.设置redis密码config set requirepass ****（****为你要设置的密码）
#连接带密码的redis数据库
./redis-cli -a 123
```



## Docker Compose入门

**Docker compose就是docker命令的一些批处理集合**

#### 1、通过docker-compose文件构建项目

![](..\docker-compose\1.png)



```yml
version: '3'

services:

  wordpress:
    image: wordpress
    ports:
      - 8080:80
    depends_on:
      - mysql
    environment:
      WORDPRESS_DB_HOST: mysql
      WORDPRESS_DB_PASSWORD: root
    networks:
      - my-bridge

  mysql:
    image: mysql:5.7
    environment:
      MYSQL_ROOT_PASSWORD: root
      MYSQL_DATABASE: wordpress
    volumes:
      - mysql-data:/var/lib/mysql
    networks:
      - my-bridge

volumes:
  mysql-data:
#定义了属性为bridge，名称为 my-bridge的网络
networks:
  my-bridge:
    driver: bridge
```

```shell
#执行命令
docker-compose -f docker-compose.yml up -d
```

**docker-compose常用命令--基本和Dokcer的命令很相似**

```yml
  build              Build or rebuild services
  config             Validate and view the Compose file
  create             Create services
  down               Stop and remove containers, networks, images, and volumes
  events             Receive real time events from containers
  exec               Execute a command in a running container
  help               Get help on a command
  images             List images
  kill               Kill containers
  logs               View output from containers
  pause              Pause services
  port               Print the public port for a port binding
  ps                 List containers
  pull               Pull service images
  push               Push service images
  restart            Restart services
  rm                 Remove stopped containers
  run                Run a one-off command
  scale              Set number of containers for a service
  start              Start services
  stop               Stop services
  top                Display the running processes
  unpause            Unpause services
  up                 Create and start containers
  version            Show the Docker-Compose version information
```

#### 2、通过Dockerfile构建

##### **docker-compose.yml编写**

```yml
version: "3"

services:

  redis:
    image: redis

  web:
    build:
      context: .
      dockerfile: Dockerfile
    ports:
      - 8080:5000
    environment:
      REDIS_HOST: redis
```

##### **Dockerfile的编写**

```dockerfile
FROM python:2.7
LABEL maintaner="Peng Xiao xiaoquwl@gmail.com"
COPY . /app
WORKDIR /app
RUN pip install flask redis
EXPOSE 5000
CMD [ "python", "app.py" ]
```

##### app.py的编写

```python
from flask import Flask
from redis import Redis
import os
import socket

app = Flask(__name__)
redis = Redis(host=os.environ.get('REDIS_HOST', '127.0.0.1'), port=6379)


@app.route('/')
def hello():
    redis.incr('hits')
    return 'Hello Container World! I have been seen %s times and my hostname is %s.\n' % (redis.get('hits'),socket.gethostname())


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
```

#### **3、docker compose --scale参数 ，通过haproxy代理，实现负载均衡**

docker-compose up --scale web=3 -d



```yml
version: "3"

services:

  redis:
    image: redis

  web:
    build:
      context: .
      dockerfile: Dockerfile
    ports: ["8080"]  #暴露对外服务的8080
    environment:
      REDIS_HOST: redis

  lb:
    image: dockercloud/haproxy
    links:
      - web
    ports:
      - 80:80  #映射app.py程序中的80端口
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock 
```



```python
#!/usr/bin/python
#coding:utf-8
from flask import Flask
from redis import Redis
import os
import socket

app = Flask(__name__)
redis = Redis(host=os.environ.get('REDIS_HOST', '127.0.0.1'), port=6379)

@app.route('/')
def hello():
    redis.incr('hits')
    return 'Hello Container World! I have been seen %s times and my hostname is %s.\n' % (redis.get('hits'),socket.gethostname())

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, debug=True)  #之前的5000改为80
```



```dockerfile
FROM python:2.7
LABEL maintaner="Peng Xiao xiaoquwl@gmail.com"
COPY . /app
WORKDIR /app
RUN pip install flask redis
EXPOSE 80  #暴露服务的80端口，与haproxy的端口对应
CMD [ "python", "app.py" ]
```

**测试：for i in `seq 10`; do curl 127.0.0.1:8080;done**，连续访问程序，可以输出不用的结果