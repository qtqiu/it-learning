# Maven

## 一、依赖调解原则

maven自动按照下边的原则调解：

##### 第一声明者优先原则

在pom文件定义依赖，先声明的依赖为准。

测试：如果将上边struts-spring-plugins和spring-context顺序颠倒，系统将导入spring-beans-4.2.4。

分析：由于spring-context在前边以spring-context依赖的spring-beans-4.2.4为准，所以最终spring-beans-4.2.4添加到了工程中。

##### 路径近者优先原则

例如：A依赖 spring-beans-4.2.4，A依赖B依赖 spring-beans-3.0.5，则spring-beans-4.2.4优先被依赖在A中，因为spring-beans-4.2.4相对spring-beans-3.0.5被A依赖的路径最近。

测试：在本工程中的pom中加入spring-beans-4.2.4的依赖，根据路径近者优先原则，系统将导入spring-beans-4.2.4

### 排除依赖

上边的问题也可以通过排除依赖方法辅助依赖调解，如下： 比如在依赖struts2-spring-plugin的设置中添加排除依赖，排除spring-beans， 下边的配置表示：依赖struts2-spring-plugin，但排除struts2-spring-plugin所依赖的spring-beans。

```xml
<!-- struts2-spring-plugin依赖spirng-beans-3.0.5 -->
  	<dependency>
  		<groupId>org.apache.struts</groupId>
  		<artifactId>struts2-spring-plugin</artifactId>
  		<version>2.3.24</version>
  		<!-- 排除 spring-beans-->
  		<exclusions>
  			<exclusion>
  				<groupId>org.springframework</groupId>
  				<artifactId>spring-beans</artifactId>
  			</exclusion>
			<exclusion>
  				<groupId>org.springframework</groupId>
  				<artifactId>spring-context</artifactId>
  			</exclusion>
  		</exclusions>
  	</dependency>
```

### 锁定版本

面对众多的依赖，有一种方法不用考虑依赖路径、声明优化等因素可以采用直接锁定版本的方法确定依赖构件的版本，版本锁定后则不考虑依赖的声明顺序或依赖的路径，以锁定的版本的为准添加到工程中，此方法在企业开发中常用。

```xml

<dependencyManagement>
  	<dependencies>
  		<!--这里锁定版本为4.2.4 -->
  		<dependency>
  			<groupId>org.springframework</groupId>
  			<artifactId>spring-beans</artifactId>
  			<version>4.2.4.RELEASE</version>
  		</dependency>
		<dependency>
  			<groupId>org.springframework</groupId>
  			<artifactId>spring-context</artifactId>
  			<version>4.2.4.RELEASE</version>
  		</dependency>
  	</dependencies>
  </dependencyManagement>
```

<font style='color:red;font-size:12;font-family:微软雅黑;'>注意：在工程中锁定依赖的版本并不代表在工程中添加了依赖，如果工程需要添加锁定版本的依赖则需要单独添加<dependencies></dependencies>标签，如下</font>

```xml
<dependencies>
  		<!--这里是添加依赖 -->
  		<dependency>
  			<groupId>org.springframework</groupId>
  			<artifactId>spring-beans</artifactId>
 		</dependency>
		<dependency>
  			<groupId>org.springframework</groupId>
  			<artifactId>spring-context</artifactId>
 		</dependency>
  	</dependencies>
```

## 二、继承和聚合

- **继承**

继承是为了消除重复，如果将dao、service、web分开创建独立的工程则每个工程的pom.xml文件中的内容存在重复，比如：设置编译版本、锁定spring的版本的等，**可以将这些重复的配置提取出来在父工程的pom.xml中定义。**

- 聚合

项目开发通常是分组分模块开发，每个模块开发完成要运行整个工程需要将每个模块聚合在一起运行，比如：dao、service、web三个工程最终会打一个独立的war运行



### 案例实现

#### 创建parent工程

- 创建父工程

- 注意，父工程只是一个pom工程

- 定义pom.xml

  在父工程的pom.xml中抽取一些重复的配置的，比如：锁定jar包的版本、设置编译版本等。 

  ```xml
  <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>com.yaolong.maven</groupId>
    <artifactId>maven-parent</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>pom</packaging>
    <name>parent project</name>
      
      <!-- 由于父工程要聚合子工程，配置modules -->
     <modules>
      <!-- 在modules中配置相对路径，相对父工程pom.xml的路径找到子工程的pom.xml -->
     	<module>maven-dao</module>
     	<module>maven-service</module>
     	<module>maven-web</module>
     </modules>
    
    <!-- 属性 -->
  	<properties>
  		<spring.version>4.2.4.RELEASE</spring.version>
  		<hibernate.version>5.0.7.Final</hibernate.version>
  		<struts.version>2.3.24</struts.version>
  	</properties>
  	<dependencyManagement>
  		<dependencies>
  			<!-- 统一依赖构件版本 -->
  			<dependency>
  				<groupId>org.springframework</groupId>
  				<artifactId>spring-context</artifactId>
  				<version>${spring.version}</version>
  			</dependency>
  			<dependency>
  				<groupId>org.springframework</groupId>
  				<artifactId>spring-aspects</artifactId>
  				<version>${spring.version}</version>
  			</dependency>
  			<dependency>
  				<groupId>org.springframework</groupId>
  				<artifactId>spring-orm</artifactId>
  				<version>${spring.version}</version>
  			</dependency>
  			<dependency>
  				<groupId>org.springframework</groupId>
  				<artifactId>spring-test</artifactId>
  				<version>${spring.version}</version>
  			</dependency>
  			<dependency>
  				<groupId>org.springframework</groupId>
  				<artifactId>spring-web</artifactId>
  				<version>${spring.version}</version>
  			</dependency>
  			<dependency>
  				<groupId>org.hibernate</groupId>
  				<artifactId>hibernate-core</artifactId>
  				<version>${hibernate.version}</version>
  			</dependency>
  			<dependency>
  				<groupId>org.apache.struts</groupId>
  				<artifactId>struts2-core</artifactId>
  				<version>${struts.version}</version>
  			</dependency>
  			<dependency>
  				<groupId>org.apache.struts</groupId>
  				<artifactId>struts2-spring-plugin</artifactId>
  				<version>${struts.version}</version>
  			</dependency>
  			<dependency>
  				<groupId>org.apache.struts</groupId>
  				<artifactId>struts2-json-plugin</artifactId>
  				<version>${struts.version}</version>
  			</dependency>
  		</dependencies>
  	</dependencyManagement>
   
  	<build>
  		<finalName>maven-web</finalName>
  		<plugins>
  			<plugin>
  				<groupId>org.apache.maven.plugins</groupId>
  				<artifactId>maven-compiler-plugin</artifactId>
  				<configuration>
  					<source>1.7</source>
  					<target>1.7</target>
  					<encoding>UTF-8</encoding>
  				</configuration>
  			</plugin>
  		</plugins>
  	</build>
    
  </project>
  ```

  将父工程发布至仓库

  父工程创建完成执行maven-install将父工程发布到仓库方便子工程继承：

  查看本地仓库，看到maven-parent已经被安装

#### 创建dao子模块

选择maven模块： 

这里指定模块名称，选择父工程，选择“跳过骨架选择”： 

定义pom.xml

**dao模块的pom.xml文件中需要继承父模块，添加持久层需要的依赖坐标**，==不需要添加版本号了==，但是额外引入的依赖jar包需要添加

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  
  <parent>
    <groupId>com.yaolong.maven</groupId>
    <artifactId>maven-parent</artifactId>
    <version>0.0.1-SNAPSHOT</version>
  </parent>
  
  <artifactId>maven-dao</artifactId>
  <packaging>jar</packaging>
  
    <dependencies>
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-core</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-aspects</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-orm</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-test</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-web</artifactId>
		</dependency>
		
		<!-- 数据库驱动 -->
 
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>5.1.6</version>
			<scope>runtime</scope>
		</dependency>
		<!-- c3p0 -->
 
		<dependency>
			<groupId>c3p0</groupId>
			<artifactId>c3p0</artifactId>
			<version>0.9.1.2</version>
		</dependency>
		<!-- 日志 -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>1.7.2</version>
		</dependency>
		<!-- junit -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.9</version>
			<scope>test</scope>
		</dependency>
	</dependencies>
</project>	
```

### 运行调试

**方法1：**

在maven-web工程的pom.xml中配置tomcat插件运行 运行maven-web工程它会从本地仓库下载依赖的jar包，所以==当maven-web依赖的jar包内容修改了必须及时发布到本地仓库==，比如：maven-web依赖的maven-service修改了，需要及时将maven-service发布到本地仓库。

**方法2：**

在父工程的pom.xml中配置tomcat插件运行，自动聚合并执行

推荐方法2，如果子工程都在本地，采用方法2则不需要子工程修改就立即发布到本地仓库，父工程会自动聚合并使用最新代码执行。

注意：如果子工程和父工程中都配置了tomcat插件，运行的端口和路径以子工程为准。


## maven私服

项目组编写了一个通用的工具类，其它项目组将类拷贝过去使用，当工具类修改bug后通过邮件发送给各各项目组，这种分发机制不规范可能导致工具类版本不统一。

解决方案：

项目组将写的工具类通过maven构建，打成jar，将jar包发布到公司的maven仓库中，公司其它项目通过maven依赖管理从仓库自动下载jar包。

公司在自己的局域网内搭建自己的远程仓库服务器，称为私服，私服服务器即是公司内部的maven远程仓库，每个员工的电脑上安装maven软件并且连接私服服务器，员工将自己开发的项目打成jar并发布到私服服务器，其它项目组从私服服务器下载所依赖的构件（jar）。

**私服还充当一个代理服务器，当私服上没有jar包会从互联网中央仓库自动下载**

### 搭建私服环境

Nexus 是Maven仓库管理器，通过nexus可以搭建maven仓库，同时nexus还提供强大的仓库管理功能，构件搜索功能等。

Nexus 提供两种安装包，一种是包含 Jetty 容器的 bundle 包，另一种是不包含容器的 war 包。

如果下载的是war包，必须要自己另外配置Servlet容器（如Tomcat）,才能运行起来。 如果下载的是bundle包，它自己内置了jetty，不用再下载其他依赖。 建议使用bundle安装包安装Nexus。

**下载Nexus**， 下载地址：http://www.sonatype.org/nexus/archived/

**安装nexus**
下载后解压到英文路径目录

cmd进入bin目录，执行nexus.bat install，注意，所有命令行操作都要自系统管理员权限的条件下

安装成功在服务中查看有nexus服务：

**卸载nexus**
cmd进入nexus的bin目录，执行：nexus.bat uninstall，然后查看window服务列表nexus,不存在了表示已被删除

**启动nexus**
方法1： cmd进入bin目录，执行nexus.bat start

方法2：直接在服务列表中启动即可 

查看nexus的配置文件conf/nexus.properties

访问http://localhost:8081/nexus/

点击右上角的log in ,默认账户==admin和admin123==

### 仓库类型

#### nexus仓库的四种类型：

**hosted**，宿主仓库，部署自己的jar到这个类型的仓库，包括releases和snapshot两部分，Releases公司内部发布版本仓库、 Snapshots 公司内部测试版本仓库

**proxy**，代理仓库，用于代理远程的公共仓库，如maven中央仓库，用户连接私服，私服自动去中央仓库下载jar包或者插件。

**group**，仓库组，用来合并多个hosted/proxy仓库，通常我们配置自己的maven连接仓库组。

**virtual**(虚拟)：兼容Maven1 版本的jar或者插件

nexus仓库默认在**sonatype-work**目录中： 

 与之对应： 

**central**：代理仓库，代理中央仓库 
	**apache-snapshots**：代理仓库 存储snapshots构件，代理地址https://repository.apache.org/snapshots/
	**central-m1**： virtual类型仓库，兼容Maven1 版本的jar或者插件
	**releases**：本地仓库，存储releases构件。
	**snapshots**：本地仓库，存储snapshots构件。
	**thirdparty**：第三方仓库
	**public**：仓库组

#### **将项目发布到私服**

需求
企业中多个团队协作开发通常会将一些公用的组件、开发模块等发布到私服供其它团队或模块开发人员使用。 本例子假设多团队分别开发dao、service、web，某个团队开发完在dao会将dao发布到私服供service团队使用，本例子会将dao工程打成jar包发布到私服。

配置
第一步：需要在客户端即部署dao工程的电脑上配置maven环境，并修改 **==settings.xml==** 文件，配置连接私服的用户和密码。此用户名和密码用于私服校验，因为私服需要知道上传都 的账号和密码 是否和私服中的账号和密码 一致。

```xml
<server>
      <id>releases</id>
      <username>admin</username>
      <password>admin123</password>
    </server>
	<server>
      <id>snapshots</id>
      <username>admin</username>
      <password>admin123</password>
    </server>
```

releases 连接发布版本项目仓库

snapshots 连接测试版本项目仓库

**第二步： 配置项目pom.xml**

配置私服仓库的地址，本公司的自己的jar包会上传到私服的宿主仓库，根据工程的版本号决定上传到哪个宿主仓库，如果版本为release则上传到私服的release仓库，如果版本为snapshot则上传到私服的snapshot仓库

```xml
<distributionManagement>
  	<repository>
  		<id>releases</id>
	<url>http://localhost:8081/nexus/content/repositories/releases/</url>
  	</repository> 
  	<snapshotRepository>
  		<id>snapshots</id>
	<url>http://localhost:8081/nexus/content/repositories/snapshots/</url>
  	</snapshotRepository> 
  </distributionManagement>
```

**注意：pom.xml这里<id> 和 settings.xml 配置 <id> 对应！**

#### 测试

将项目dao工程打成jar包发布到私服：

1、首先启动nexus

2、对dao工程执行deploy命令

根据本项目pom.xml中version定义决定发布到哪个仓库，如果version定义为snapshot，执行deploy后查看nexus的snapshot仓库，如果version定义为release则项目将发布到nexus的release仓库，本项目将发布到snapshot仓库

## 从私服下载jar包

**需求**
没有配置nexus之前，如果本地仓库没有，去中央仓库下载，通常在企业中会在局域网内部署一台私服服务器，有了私服本地项目首先去本地仓库找jar，如果没有找到则连接私服从私服下载jar包，如果私服没有jar包私服同时作为代理服务器从中央仓库下载jar包，这样做的好处是一方面由私服对公司项目的依赖jar包统一管理，一方面提高下载速度，项目连接私服下载jar包的速度要比项目连接中央仓库的速度快的多。

本例子测试从私服下载dao 工程jar包。

**管理仓库组**
nexus中包括很多仓库，hosted中存放的是企业自己发布的jar包及第三方公司的jar包，proxy中存放的是中央仓库的jar，为了方便从私服下载jar包可以将多个仓库组成一个仓库组，每个工程需要连接私服的仓库组下载jar包。

**打开nexus配置仓库组**

上图中仓库组包括了本地仓库、代理仓库等。

**在setting.xml中配置仓库**
在客户端的setting.xml中配置私服的仓库，由于setting.xml中没有repositories的配置标签需要使用profile定义仓库。

```xml
<profile>   
	<!--profile的id-->
   <id>dev</id>   
    <repositories>   
      <repository>  
		<!--仓库id，repositories可以配置多个仓库，保证id不重复-->
        <id>nexus</id>   
		<!--仓库地址，即nexus仓库组的地址-->
        <url>http://localhost:8081/nexus/content/groups/public/</url>   
		<!--是否下载releases构件-->
        <releases>   
          <enabled>true</enabled>   
        </releases>   
		<!--是否下载snapshots构件-->
        <snapshots>   
          <enabled>true</enabled>   
        </snapshots>   
      </repository>   
    </repositories>  
	 <pluginRepositories>  
    	<!-- 插件仓库，maven的运行依赖插件，也需要从私服下载插件 -->
        <pluginRepository>  
        	<!-- 插件仓库的id不允许重复，如果重复后边配置会覆盖前边 -->
            <id>public</id>  
            <name>Public Repositories</name>  
            <url>http://localhost:8081/nexus/content/groups/public/</url>  
        </pluginRepository>  
    </pluginRepositories>  
  </profile>
```

使用profile定义仓库需要激活才可生效。

```xml
<activeProfiles>
    <activeProfile>dev</activeProfile>
  </activeProfiles>
```

