### POI和EasyExcel的用法

#### 1、**POI的说明**

![](F:\学习笔记\poi_easyExcel\assert\1.png)

- HSSF ---2003版的Excel     **.lsx**
- <u>XSSF ---2007版的Excel</u>     **.xlsx**
- HWPF ---Word的操作
- HSLF --PDF 的操作
- HDGF --Visio的操作

POI官网： https://poi.apache.org/      原生，会比较麻烦

EasyExcel： https://github.com/alibaba/easyexcel     

**官方文档**： https://www.yuque.com/easyexcel/doc/read  很好的demo例子

![](F:\学习笔记\poi_easyExcel\assert\2.png)



**导入POI依赖**

![](F:\学习笔记\poi_easyExcel\assert\3微信图片_20200523105154.png)

#### 2、Excel的一些概念

![](F:\学习笔记\poi_easyExcel\assert\4.png)



**java代码**

```java
    private static final String PATH = "E:\\aaa\\";

    @Test
    public void testWrite03() throws Exception {

        // 工作簿
        Workbook wk = new HSSFWorkbook();
        // 工作表
        Sheet sheet = wk.createSheet("表名1");

        // 创建行 ,从0开始
        Row row1 = sheet.createRow(0);

        //行row1里面创建单元格（1,1）
        Cell cell1 = row1.createCell(0);
        cell1.setCellValue("今天新增观众");
//        （1,2）
        Cell cell2 = row1.createCell(1);
        cell2.setCellValue(666);

        // 创建行 第二行
        Row row2 = sheet.createRow(1);
        //第2行row2里面创建第一个单元格（2,1）
        Cell cell3 = row2.createCell(0);
        cell3.setCellValue("统计时间：");
//     （2,2）
        Cell cell4 = row2.createCell(1);

        String time = new DateTime().toString("yyyy-MM-dd HH:mm:ss");
        cell4.setCellValue(time);

        FileOutputStream fout = new FileOutputStream(PATH + "text2003.xls");
        wk.write(fout);
        fout.close();

    }
```



```java
 // 与03版本的区别是new的对象[XSSFWorkbook]和后缀名xlsx 不一样
    @Test
    public void testWrite07() throws Exception {

        // 工作簿
        Workbook wk = new XSSFWorkbook();
        // 工作表
        Sheet sheet = wk.createSheet("表名1");

        // 创建行 ,从0开始
        Row row1 = sheet.createRow(0);

        //行row1里面创建单元格（1,1）
        Cell cell1 = row1.createCell(0);
        cell1.setCellValue("今天新增观众");
//        （1,2）
        Cell cell2 = row1.createCell(1);
        cell2.setCellValue(666);

        // 创建行 第二行
        Row row2 = sheet.createRow(1);
        //第2行row2里面创建第一个单元格（2,1）
        Cell cell3 = row2.createCell(0);
        cell3.setCellValue("统计时间：");
//     （2,2）
        Cell cell4 = row2.createCell(1);

        String time = new DateTime().toString("yyyy-MM-dd HH:mm:ss");
        cell4.setCellValue(time);

        FileOutputStream fout = new FileOutputStream(PATH + "text2007.xlsx");
        wk.write(fout);
        fout.close();

    }
```

