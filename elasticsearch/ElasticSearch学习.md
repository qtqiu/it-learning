## ElasticSearch学习

### 1、基本知识概念

官网：https://www.elastic.co/cn/

![](F:\学习笔记\elasticsearch\assert\1.png)

#### 目录结构

![](F:\学习笔记\elasticsearch\assert\2目录结构.png)

#### Head插件安装





![](F:\学习笔记\elasticsearch\assert\3安装插件.png)

![](F:\学习笔记\elasticsearch\assert\4.png)

### 2、了解ELK

![](F:\学习笔记\elasticsearch\assert\5ELK的说明.png)



#### 安装Kibana

![](F:\学习笔记\elasticsearch\assert\6Kibana安装.png)

下载tar.gz文件，解压（文件有点大）--需要等待一些时间



##### Kibana汉化（在kibana.yml配置i18n.locale）

![](F:\学习笔记\elasticsearch\assert\7汉化.png)

### 3、ES的名称概念【面向文档的数据库】

![](F:\学习笔记\elasticsearch\assert\8ES概念.png)

![](F:\学习笔记\elasticsearch\assert\8ES概念2.png)

#### （1）、物理设计

![](F:\学习笔记\elasticsearch\assert\9物理设计.png)

#### （2）、逻辑设计

![](F:\学习笔记\elasticsearch\assert\10逻辑设计.png)

#### 倒排索引

![](F:\学习笔记\elasticsearch\assert\11副本数.png)



![](F:\学习笔记\elasticsearch\assert\12倒排索引.png)

![](F:\学习笔记\elasticsearch\assert\12倒排索引2.png)

#### IK分词器的安装

github地址：https://github.com/medcl/elasticsearch-analysis-ik/releases



![](F:\学习笔记\elasticsearch\assert\13ik分词器的安装.png)

把下载好的文件解压到 **/home/test1/mydata/elasticsearch/plugins/ik**，elasticsearch文件夹的plugins下面

![](F:\学习笔记\elasticsearch\assert\13ik分词器的说明.png)

#### 自定义分词器字典

![](F:\学习笔记\elasticsearch\assert\13自定义分词器字典.png)

#### **Kibana测试**

![](F:\学习笔记\elasticsearch\assert\14分词器的测试.png)



## 4、Restf风格说明（**重点）

![](F:\学习笔记\elasticsearch\assert\15restful风格.png)

#### 1、创建索引测试

![](F:\学习笔记\elasticsearch\assert\16基础测试.png)

#### 2、ES的基本数据类型

![](F:\学习笔记\elasticsearch\assert\17数据类型.png)

**自定义索引里面的文档规则**

![]()![18put定义文档规则](F:\学习笔记\elasticsearch\assert\18put定义文档规则.png)



**不指定指定数据类型，则es默认分配**

![](F:\学习笔记\elasticsearch\assert\19默认指定数据类型.png)



#### 3、ES通过 get _cat/ 可获得es的当前信息

**==GET _cat/indices?v==**

![](F:\学习笔记\elasticsearch\assert\20_cat可获得es的很多信息.png)



#### 4、修改文档【两种方法】 PUT第一次是创建，第二次是修改

![](F:\学习笔记\elasticsearch\assert\21PUT修改即覆盖.png)

#### 5、第二种修改文档的方法

![](F:\学习笔记\elasticsearch\assert\22另外一种更新文档.png)

#### 6、删除索引

![](F:\学习笔记\elasticsearch\assert\23删除索引.png)



#### 7、PUT方式修改数据和POST   _update的方式修改区别

![](F:\学习笔记\elasticsearch\assert\24put方式修改数据.png) 

![](F:\学习笔记\elasticsearch\assert\24post方法的_update和put方法区别.png)

#### 8、带条件的查询 _search?q=name:张三

![](F:\学习笔记\elasticsearch\assert\25待条件的查询.png)

### 9、ES的复杂操作搜索查询

![](F:\学习笔记\elasticsearch\assert\27ES的复杂操作搜索.png)

#### 1、分值权重的说明

![](F:\学习笔记\elasticsearch\assert\26分值权重.png)

####   2、查询出的结果说明

![](F:\学习笔记\elasticsearch\assert\28名称搜索query就是q的意思.png)

####  3、查询结果进行过滤返回，在_source里面筛选出想要的字段

![](F:\学习笔记\elasticsearch\assert\29输出结果过滤.png)

####  4、排序操作

![](F:\学习笔记\elasticsearch\assert\30排序.png)

####   5、分页查询

![](F:\学习笔记\elasticsearch\assert\31分页查询.png)

####   6、bool的多条件查询 must=and  \	should=or  \   must_not =not

![](F:\学习笔记\elasticsearch\assert\32bool和must的多条件查询.png)



![](F:\学习笔记\elasticsearch\assert\33must_not.png)



####   7、用Filter过滤器进行操作



![](F:\学习笔记\elasticsearch\assert\34用filter进行过滤操作.png)

![](F:\学习笔记\elasticsearch\assert\35条件大于等于gte_lte.png)

####  8、匹配多个条件查询 match

![](F:\学习笔记\elasticsearch\assert\36.png)



#### 9、精确查询 Term & Text和keyword的区别

![](F:\学习笔记\elasticsearch\assert\37精确查询.png)

   **text**：会被分词器解析

==**keyword**：是一个整体，不会被分词器解析==

例子：创建两个字段name和desc，name是text；desc是关键字。所以

![](F:\学习笔记\elasticsearch\assert\37测试text和keyword的区别.png)

**是Text类型，所以会被分词器解析出各个单词

![](F:\学习笔记\elasticsearch\assert\37分词器说明.png)



![](F:\学习笔记\elasticsearch\assert\37测试text和keyword的区别2.png)

#### 10、精确多条件查询

![](F:\学习笔记\elasticsearch\assert\38精确查询多个值.png)



#### 11、高亮查询 highlight，自定义高亮字段查询

![](F:\学习笔记\elasticsearch\assert\39高亮显示查询.png)

![](F:\学习笔记\elasticsearch\assert\40自定义高亮查询.png)



> 练习的一些命令笔记

```json
GET _search
{
  "query": {
    "match_all": {}
  }
}


GET _cat/indices?v


GET jd_goods/_search
{
  "query": {
   "match_all": {}
    
  }
}

POST qiu/_doc/5
{
  "doc":{
    "name":"chenliu3333",
    "age":123,  
    "adress":"广东省中山市石歧区33333h"
  }
}

#修改制定字段
POST qiu/_doc/3/_update
{
  "doc":{
    "name":"chenliu1111",

  "adress":"广东省中山市石歧区222222"
  }
}


GET qiu/_doc/_search
{
  "query":
  {
    "match_all":{}
  }
}

# 查找某一条数据，并且筛选出的返回值
GET qiu/_search
{
  "query":
  {
    "match":{
      "name":"zhangsan"
    }
  },
  "_source":["age","adress"]
}


GET qiu

GET qiu/_search
{
  "query":
  {
    "match":{
      "name":"chenliu"
    }
  },
  "_source":["age","adress"]
}

GET qiu/_doc/_search
{
  "query":
  {
    "match":{
      "name":"chegnliu"
    }
  },
  "_source":["age","adress"]
}




GET qiu/_doc/_search?q=name:zhangsan

GET qiu/_doc/3

#最少切分
GET _analyze 
{
  "analyzer":"ik_smart",
  "text":"带我飞有限公司"
}

#最细粒度划分
GET _analyze
{
   
  "analyzer":"ik_max_word",
  "text":"带我飞有限公司"
}

#######创建索引并指定字段类型
PUT testdiff
{
  "mappings":{
    "properties":{
    "name":{
      "type":"text"
    },
       "desc":{
      "type": "keyword"
    }
   }
  }
}

GET testdiff/_search
{
  "query":{
    "match_all":{}
  }
}

PUT testdiff/_doc/1
{
  "name":"herry说java name",
   "desc":"mary说java name"
}


GET testdiff/_search
{
  "query":{
    "term":{
      "name":"herry"
    }
  }
}

GET testdiff/_search
{
  "query":{
    "term":{
      "desc":"mary说java name"
    }
  }
}
############################


####高亮显示===

GET testdiff/_search
{
  "query":{
    "match":{
      "name":"java"
    }
  },
   "highlight":{
    "fields":{
      "name":{}
    }
  }

}

###### 查看索引结构
GET gao_index/_mapping?pretty
```

